package ep2;

public class YellowEnemy extends Enemy {

	private int walklate=0;
	private static int DOWN = 3;
	private static int UP = -3;
	private static int LEFT = -3;
	private static int RIGHT = 3;
	private int dir;
	
	public YellowEnemy(int x, int y, Sprite target) {
		super(x, y, target);
		loadImage("images/YellowOVNI.gif");
		shotlate = 150;
		bulletspeed = 2;
	}

	
	public void Update() {
		if (0 == walklate) {
			if (target.getY() > y) {
				dir = DOWN;
			} else {
				dir = UP;
			}
		}
		else if (walklate < 25) {
			move(0, dir);
		}
		else if (walklate == 50) {
			if (target.getX() > x) {
				dir = RIGHT;
			} else {
				dir = LEFT;
			}
		} else if (50 < walklate && walklate < 75) {
			move(dir, 0);
		} else if (walklate > 100) {
			walklate = -1;
		}
		walklate++;
		
		super.Update();
	}
}
