package ep2;

public class Fix extends Sprite {
	private static final double MAXSPEED = 1;
	private static final int RIGHT=1, LEFT=2;
	private int direction = RIGHT;
	
	public Fix(int x, int y) {
		super(x, y);
		loadImage("images/Fix.gif");
	}

	public void move () {
		if (direction == RIGHT) {
			super.move(MAXSPEED, MAXSPEED/2);
			if (x > Game.getWidth() - getWidth()) direction = LEFT;
		} else if (direction == LEFT) {
			super.move(-MAXSPEED, MAXSPEED/2);
			if (x < 0) direction = RIGHT;
		}
	}
}
