package ep2;

import java.awt.Rectangle;
import java.awt.Image;
import javax.swing.ImageIcon;

public abstract class Sprite {

    protected int x;
    protected int y;
    protected int width;
    protected int height;
    protected boolean visible;
    protected Image image;

    public Sprite(int x, int y) {

        this.x = x;
        this.y = y;
        visible = true;
    }

    protected void loadImage(String imageName) {

        ImageIcon ii = new ImageIcon(imageName);
        image = ii.getImage();
        getImageDimensions();
    }
    
    protected void getImageDimensions() {

        width = image.getWidth(null);
        height = image.getHeight(null);
    }    

    public Image getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public int getWidth(){
        return width;
    }
    
    public int getHeight(){
        return height;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean enable) {
        visible = enable;
    }
    
    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }
    
    private int lx;
    private int ly;
    private int timerx=1, timery=1;
    
    public void move(double x, double y) {
    	
    	if (Math.abs(x)>1) this.x+=x;
    	else {
    		if (Math.abs(x)>0.15) {
	    		if (Math.abs(x)>0.8) lx=1;
		    	else if (Math.abs(x)>0.45) lx=2;
		    	else if (Math.abs(x)>0.3) lx=3;
		    	else if (Math.abs(x)>0.23) lx=4;
		    	else if (Math.abs(x)>0.19) lx=5;
		    	else lx=6;
	    	} else lx=0;
	    	
	    	
	    	if(lx>0) {
	    		if (timerx>lx) {
	    			if (x>0) this.x++;
	    			else this.x--;
	    			timerx=2;
	    		} else timerx++;
	    	}	
    	}
    	
    	if (Math.abs(y)>1) this.y+=y;
    	else {
	    	if (Math.abs(y)>0.8) ly=1;
	    	else if (Math.abs(y)>0.45) ly=2;
	    	else if (Math.abs(y)>0.3) ly=3;
	    	else if (Math.abs(y)>0.23) ly=4;
	    	else if (Math.abs(y)>0.19) ly=5;
	    	else if (Math.abs(y)>0.15) ly=6;
	    	
	    	if(ly>0) {
	    		if (timery>ly) {
	    			if (y>0) this.y++;
	    			else this.y--;
	    			timery=2;
	    		} else timery++;
	    	}	
    	}
    }
}