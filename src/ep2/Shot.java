package ep2;

public class Shot extends Sprite {
	
	private String type;
	
	public Shot(int x, int y, String type) {
		super(x, y);
		this.type = type;
		loadImage("images/" + type + ".gif");
	}
	
	public void SetX(int x) {
		this.x = x;
	}
	
	public void SetY(int y) {
		this.y = y;
	}
	
	public void move() {
		switch (type) {
			case "ShootRay": {
				y-=4;
				break;
			}
			case "BulletV": {
				y-=4;
				break;
			}
			case "BulletL": {
				y-=3;
				x-=1;
				break;
			}
			case "BulletR": {
				y-=3;
				x+=1;
				break;
			}
		}
 	}
}
