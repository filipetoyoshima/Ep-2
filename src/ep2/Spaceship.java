package ep2;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 2;
    private static final int MAX_SHIELD = 100;
    private static final int SHOT_LATE = 10;
   
    private int shield;
    private int score;
    private int shotlate;
    private boolean key_up;
    private boolean key_down;
    private boolean key_left;
    private boolean key_right;
    private boolean key_shot;
    protected Shot exampler;
    protected List <Shot> bullet;

    public Spaceship(int x, int y) {
        super(x, y);
        
        initSpaceShip();
        shield = MAX_SHIELD;
        score = 0;
        exampler = new Shot(-1, -1, "ShootRay");
        bullet = new ArrayList<>();
        key_up = false;
        key_down = false;
        key_left = false;
        key_right = false;
        key_shot = false;
        shotlate = SHOT_LATE - 1;
    }

    private void initSpaceShip() {
        
        noThrust();
        
    }
    
    private void noThrust(){
        loadImage("images/SpaceshipWhiteNoT.gif"); 
    }
    
    private void thrust(){
        loadImage("images/SpaceshipWhiteWT.gif"); 
    }    

    public void move() {
        
        // Limits the movement of the spaceship to the side edges.
        if(x <= 0){
            x=1;
        } else if (x + getWidth() > Game.getWidth()) {
        	x = Game.getWidth()-1 - getWidth();
        }
        
        // Moves the spaceship on the horizontal axis
        if (key_left) x-=MAX_SPEED_X;
        if (key_right) x+=MAX_SPEED_X;
        
        // Limits the movement of the spaceship to the vertical edges.
        if(y <= 0) {
        	y=1;
        } else if (y + getHeight() > Game.getHeight()) {
        	y = Game.getHeight() - getHeight();
        }

        // Moves the spaceship on the vertical axis
        if (key_up) y-=MAX_SPEED_Y;
        if (key_down) y+=MAX_SPEED_Y;
        
        // Shots
        if (key_shot) {
        	shotlate++;
        	if (shotlate == SHOT_LATE) {
        		Shot();
        		shotlate = 0;
        	}
        }
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        // Set speed to move to the left
        if ((key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) && key_left == false) { 
            key_left = true;
        }

        // Set speed to move to the right
        if ((key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) && key_right == false) {
            key_right = true;
        }
        
        // Set speed to move to up and set thrust effect
        if ((key == KeyEvent.VK_UP || key == KeyEvent.VK_W) && key_up == false) {
            key_up = true;
            thrust();
        }
        
        // Set speed to move to down
        if ((key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) && key_down == false) {
            key_down = true;
        }
        
        if (key == KeyEvent.VK_SPACE) {
            key_shot = true;
        }
        
    }
    
    public void Shot () {
    	this.bullet.add(new Shot(x+((image.getWidth(null))/2)-(exampler.image.getWidth(null)/2), y+image.getHeight(null)/2, "ShootRay"));
        /*	
       	this.bullet.add(new Shot(x+((image.getWidth(null))/2)-(exampler.image.getWidth(null)/2), y+image.getHeight(null)/2, "BulletV"));
       	this.bullet.add(new Shot(x+((image.getWidth(null))/2)-(exampler.image.getWidth(null)/2), y+image.getHeight(null)/2, "BulletL"));
        this.bullet.add(new Shot(x+((image.getWidth(null))/2)-(exampler.image.getWidth(null)/2), y+image.getHeight(null)/2, "BulletR"));
         */
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) {
            key_left = false;
        }
        if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
        	key_right = false;
        }
        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_W) {
            key_up = false;
            noThrust();
        }
        if (key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) {
        	key_down = false;
        }
        if (key == KeyEvent.VK_SPACE) {
        	key_shot = false;
        	shotlate = SHOT_LATE - 1;
        }
    }
    
    public void addScore(int bonus) {
    	score+=bonus;
    }
    
    public int getScore() {
    	return score;
    }
    
    public int getShield() {
    	return shield;
    }
    
    public void TakeDamage(int damage) {
    	shield-=damage;
    	if(shield<0) shield = 0;
    }
    
    public void RecoveryShield() {
    	shield+=35;
    	if(shield>MAX_SHIELD) shield = MAX_SHIELD;
    }
 }