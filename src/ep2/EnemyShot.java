package ep2;

public class EnemyShot extends Shot {

	private double speedX, speedY;
	
	public EnemyShot(int x, int y, String type, Sprite target, double speed) {
		
		super(x, y, type);
		int difx, dify;
		double module;
		difx = target.getX() - x;
		dify = target.getY() - y;
		module = Math.sqrt((difx * difx)+(dify * dify));
		if (module != 0) {
			speedX = difx*speed/module;
			speedY = dify*speed/module;
		}
	}
	
	public void move() {
		super.move(speedX, speedY);
	}
	
	public boolean isOut() {
		if (x > Game.getWidth() || x < 0 || y > Game.getHeight() || y < 0) return true;
		else return false;
	}
}
