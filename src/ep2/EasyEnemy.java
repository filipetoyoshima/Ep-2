package ep2;

public class EasyEnemy extends Enemy {
	
	private static int BULLETSPEED=1;
	
	public EasyEnemy(int x, int y, Sprite target) {
		super(x, y, target);
		loadImage("images/GreenOVNI.gif");
		shotlate = 200;
		bulletspeed = BULLETSPEED;
	}
	
	public void Update () {
        y+=1;
        super.Update();
	}
}