package ep2;

public class HardEnemy extends Enemy {

	private static final double MAX_SPEED = 1; 
	private static final double BULLETSPEED = 2.5;
	private int difx, dify;
	private double vx, vy, module;
	
	public HardEnemy(int x, int y, Sprite target) {
		super(x, y, target);
		loadImage("images/BlueOVNI.gif");
		shotlate = 200;
		bulletspeed = BULLETSPEED;
	}
	
	public void Update() {
		stalk(target);
		super.Update();
	}
	
	private void stalk (Sprite follow) {
		difx = follow.getX()+follow.getWidth()/2 - (x + getWidth()/2);
		dify = follow.getY()+follow.getHeight()/2 - (y + getHeight()/2);
		module = Math.sqrt((difx * difx)+(dify * dify));
		if (module != 0) {
			vx = difx*MAX_SPEED/module;
			vy = dify*MAX_SPEED/module;
			move(vx, vy);
		}
	}
}
