package ep2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {

	private static int EASY=1;
	private static int MEDIUM=2;
	private static int HARD=3;
	private int difficult=0;
    private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private final Timer timer_map;
    
    private Image background;
    private Image lifebar;
    private Spaceship spaceship;
    private boolean start;
    private List <Enemy> alien;
    private List <EnemyShot> enemyshot;
    private List <Fix> lifebonus;
    private List <Astronaut> scorebonus;
    private List <Explosion> boom;
    private Selecter easylevel;
    private Selecter mediumlevel;
    private Selecter hardlevel;
    private boolean ispaused = false;
 
    public Map() {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        ImageIcon ii = new ImageIcon("images/ShieldPowerBar.gif");
        this.background = image.getImage();
        this.lifebar = ii.getImage();
        timer_map = new Timer(Game.getDelay(), this);
        InitAttributes();
        
     }
    
   private void InitAttributes()  {
	    spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        easylevel = new Selecter(0, 0, "EasyMissionSign.gif");
        easylevel.setPosition(Game.getWidth()/4 - easylevel.getWidth()/2, Game.getHeight()/5);
        mediumlevel = new Selecter(0, 0, "MediumMissionSign.gif");
        mediumlevel.setPosition(Game.getWidth()/2 - mediumlevel.getWidth()/2, Game.getHeight()/5);
        hardlevel = new Selecter(0, 0, "HardMissionSign.gif");
        hardlevel.setPosition(3 * Game.getWidth() / 4 - hardlevel.getWidth()/2, Game.getHeight()/5);
        alien = new ArrayList<>();
        enemyshot = new ArrayList<>();
        lifebonus = new ArrayList<>();
        scorebonus = new ArrayList<>();
        boom = new ArrayList<>();
        timer_map.start();
        start = false;
   }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);
        draw(g);
        drawData(g);
        if (spaceship.getShield()<=0) drawGameOver(g);   

        Toolkit.getDefaultToolkit().sync();
    }

    private void draw(Graphics g) {
               
        g.drawImage(easylevel.getImage(), easylevel.getX(), easylevel.getY(), this);
        g.drawImage(mediumlevel.getImage(), mediumlevel.getX(), mediumlevel.getY(), this);
        g.drawImage(hardlevel.getImage(), hardlevel.getX(), hardlevel.getY(), this);
        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
        for (int i=0; i<spaceship.bullet.size(); i++) {
        	g.drawImage(spaceship.bullet.get(i).getImage(), spaceship.bullet.get(i).getX(), spaceship.bullet.get(i).getY(), this);
        }
        for (int i=0; i<scorebonus.size(); i++) {
        	g.drawImage(scorebonus.get(i).getImage(), scorebonus.get(i).getX(), scorebonus.get(i).getY(), this);
        }
        for (int i=0; i<boom.size(); i++) {
        	g.drawImage(boom.get(i).getImage(), boom.get(i).getX(), boom.get(i).getY(), this);
        }
        for (int i=0; i<alien.size(); i++) {
        	g.drawImage(alien.get(i).getImage(), alien.get(i).getX(), alien.get(i).getY(), this);
        }
        for (int i=0; i<enemyshot.size(); i++) {
        	g.drawImage(enemyshot.get(i).getImage(), enemyshot.get(i).getX(), enemyshot.get(i).getY(), this);
        }
        for (int i=0; i<lifebonus.size(); i++) {
        	g.drawImage(lifebonus.get(i).getImage(), lifebonus.get(i).getX(), lifebonus.get(i).getY(), this);
        }
    }
    
    private void drawData(Graphics g) {
    	Font font = new Font("Syncopate", Font.BOLD, 15);
    	g.setColor(Color.white);
    	g.setFont(font);
    	//g.drawString("Shield Power: " + spaceship.getShield(), 15, 15);
    	for (int i=0; i<spaceship.getShield(); i++) {
    		g.drawImage(this.lifebar, 15+(i*2), 15, null);
    	}
    	g.drawString("Score: " + spaceship.getScore(), 15, 45);
    	FontMetrics metric = getFontMetrics(font);
    	if (ispaused) {
    		g.drawString("PAUSED", (Game.getWidth() - metric.stringWidth("PAUSED")) / 2, (Game.getHeight() / 2)-20);
    		g.drawString("Press Enter to Restart", (Game.getWidth() - metric.stringWidth("Press Enter to Restart")) / 2, (Game.getHeight() / 2)+20);
    	}
    	
    }
     
    private int wavetimer=300, wavelimit=400, minlimit=50;
    private int comefix=0, comefixlimit=1500;
    private int astronauttimer=1800, astronauttimerlimit=2000;
    @Override
    public void actionPerformed(ActionEvent e) {
    	if (!ispaused) Update();
    }
    
    private void Update() {
    	if (wavetimer > wavelimit) {
    		generateEnemyWave(difficult);
    		wavetimer=0;
    		if (wavelimit>minlimit) wavelimit-=3;
    	}
    	
    	if (spaceship.getShield() > 0) {
	        updateSpaceship();
	        
	        if (start) {
	        	updateObjects();
	        	if (comefix >= comefixlimit) {
	        		generateFixBonus();
	        		comefix=0;
	        	} else comefix++;
	        	if (astronauttimer >= astronauttimerlimit) {
	        		generateAstronaut();
	        		astronauttimer = 0;
	        	} else astronauttimer++;
	        	colisor();
	         	wavetimer++;
	        }
	        else {
	        	selectOption();
	        }
	        repaint();
    	} else {
    		//Registrar pontua��o
    	}
    }
    
    /*private void dranMissionAccomplished(Graphics g) {

        String message = "MISSION ACCOMPLISHED";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }*/
    
    private void drawGameOver(Graphics g) {

        String message = "Game Over";
        String pressenter = "Press Enter to Try Again";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        g.drawString(pressenter, (Game.getWidth() - metric.stringWidth(pressenter)) / 2, (Game.getHeight() / 2)+40);
    }
    
    private void generateEnemyWave(int dif) {
    	Random sort = new Random();
    	int levelmin=0, levelmax=3;
    	switch (dif) {
	    	case 1: { //EASY
	    		levelmin = 0;
	    		levelmax = 3;
	    		break;
	    	}
	    	case 2: { //MEDIUM
	    		levelmin = 2;
	    		levelmax = 4;
	    		break;
	    	}
	    	case 3: { //HARD
	    		levelmin = 4;
	    		levelmax = 7;
	    	}
    	}
    	int wave = sort.nextInt(levelmax-levelmin+1) + levelmin;
    	switch (wave) {
    	case 0: generateEasy('R', sort.nextInt(4)+3); break;
    	case 1: generateEasy('L', sort.nextInt(4)+3); break;
    	case 2: generateEasy('F', 10); break;
    	case 3: generateMedium('V', sort.nextInt(5)+3); break;
    	case 4: generateMedium('H', sort.nextInt(5)+3); break;
    	case 5: generateHard(sort.nextInt(3)+1); break;
    	case 6: generateHard(sort.nextInt(4)+3); break;
    	case 7: generateYellow(sort.nextInt(4)+4); break;
    	}
    }
    
    private void generateEasy(char dir, int q) {
    	Random rand = new Random();
    	int initX, initY=0;
    	if (dir == 'R') {
    		initX = rand.nextInt(Game.getWidth()/2) + 10;
    		for (int i=0; i<q; i++) {
    			alien.add(new EasyEnemy(initX, initY, spaceship));
    			initX+=30;
    			initY-=30;
    		}
    	}
    	else if (dir == 'L') {
    		initX = rand.nextInt(Game.getWidth()/2) + Game.getWidth()/2 - 10;
    		for (int i=0; i<q; i++) {
    			alien.add(new EasyEnemy(initX, initY, spaceship));
    			initX-=30;
    			initY-=30;
    		}
    	}
    	else {
    		initX = rand.nextInt(Game.getWidth()-(320)) +20;
    		for (int i=0; i<10; i++) {
    			alien.add(new EasyEnemy(initX, initY, spaceship));
    			initX+=30;
    		}
    	}
    }
    
    private void generateMedium(char dir, int q) {
    	Random rand = new Random();
    	int initX;
    	int initY = 0;
    	switch (dir) {
	    	case 'V': {
	    		initX = rand.nextInt(Game.getWidth()-60)+30;
	    		for (int i=0; i<q; i++) {
	    			alien.add(new MediumEnemy(initX, initY, spaceship));
	    			initY-=30;
	    		}
	    		break;
	    	}
	    	case 'H': {
	    		initX = rand.nextInt(Game.getWidth()-(40*q)-60)+30;
	    		for (int i=0; i<q; i++) {
	    			alien.add(new MediumEnemy(initX, initY, spaceship));
	    			initX+=40;
	    		}
	    		break;
	    	}
    	}
    		
    }
    
    private void generateHard(int q) {
    	Random rand = new Random();
    	int initX, initY=0;
    	for (int i=0; i<q; i++) {
    		initX = rand.nextInt(Game.getWidth() - 50) + 25;
    		alien.add(new HardEnemy(initX, initY, spaceship));
    	}
    }
    
    private void generateYellow(int q) {
    	Random rand = new Random();
    	int initX;
    	for (int i=0; i<q; i++) {
    		initX = rand.nextInt(Game.getWidth() - 50) + 25;
    		alien.add(new YellowEnemy(initX, 0, spaceship));
    	}
    }
    
    private void generateFixBonus () {
    	Random rand = new Random();
    	lifebonus.add(new Fix(rand.nextInt(Game.getWidth()-40)+20, 0));
    }
    
    private void generateAstronaut() {
    	Random rand = new Random();
    	scorebonus.add(new Astronaut(rand.nextInt(Game.getWidth() - 100)+50, 0));
    }
    
    private void updateSpaceship() {
        spaceship.move();
        for (int i=0; i<spaceship.bullet.size(); i++) {
        	spaceship.bullet.get(i).move();
        	if (spaceship.bullet.get(i).getY() < 0) spaceship.bullet.remove(i);
        }
    }
    
    private void updateObjects() {
    	if (easylevel.isVisible()) {
    		easylevel.move(0, 8);
    		if (easylevel.getY() > Game.getHeight()) easylevel.setVisible(false);
    	}
    	if (mediumlevel.isVisible()) {
    		mediumlevel.move(0, 8);
    		if (mediumlevel.getY() > Game.getHeight()) mediumlevel.setVisible(false);
    	}
    	if (hardlevel.isVisible()) {
    		hardlevel.move(0, 8);
    		if (hardlevel.getY() > Game.getHeight()) hardlevel.setVisible(false);
    	}
    	for (int i=0;i<alien.size();i++) {
    		alien.get(i).Update();
    		if (alien.get(i).getY() > Game.getWidth()) {
    			alien.remove(i);
    		} else {
    			if (alien.get(i).ShotIsReady()) enemyshot.add(new EnemyShot(alien.get(i).getX(), alien.get(i).getY(), "AlienShot", spaceship, alien.get(i).getBulletSpeed()));
    		}
    	}
    	for (int i=0; i<enemyshot.size(); i++) {
    		enemyshot.get(i).move();
    		if (enemyshot.get(i).isOut()) enemyshot.remove(i);
    	}
    	for (int i=0; i<lifebonus.size(); i++) {
    		lifebonus.get(i).move();
    		if (lifebonus.get(i).getY() > Game.getHeight()) lifebonus.remove(i);
    	}
    	for (int i=0; i<scorebonus.size(); i++) {
    		scorebonus.get(i).move();
    		if (scorebonus.get(i).getY() > Game.getHeight()) scorebonus.remove(i);
    	}
    	for (int i=0; i<boom.size(); i++) {
    		if (boom.get(i).Finish()) boom.remove(i);
    	}
    }
    
    private void selectOption() {
    	Rectangle OptionEasy = easylevel.getBounds();
    	Rectangle OptionMedium = mediumlevel.getBounds();
    	Rectangle OptionHard = hardlevel.getBounds();
    	Rectangle shot;
    	for (int i=0; i<spaceship.bullet.size(); i++) {
    		shot = spaceship.bullet.get(i).getBounds();
    		if (shot.intersects(OptionEasy)) {
    			difficult = EASY;
    			wavelimit = 400;
    			minlimit = 100;
    			spaceship.bullet.remove(i);
    			start = true;
    			break;
    		}
    		if (shot.intersects(OptionMedium)) {
    			difficult = MEDIUM;
    			wavelimit = 300;
    			minlimit = 50;
    			spaceship.bullet.remove(i);
    			start = true;
    			break;
    		}
    		if (shot.intersects(OptionHard)) {
    			difficult = HARD;
    			wavelimit = 200;
    			minlimit = 30;
    			spaceship.bullet.remove(i);
    			start = true;
    			break;
    		}
    	}
    }
    
    private void colisor() {
    	Rectangle enemy;
    	Point shippoint = new Point(spaceship.getX()+spaceship.getWidth()/2, spaceship.getY()+spaceship.getHeight()/2);
    	for (int i=0; i<alien.size(); i++) {
    		enemy = alien.get(i).getBounds();
    		if (ColidesBullet(enemy)) {
    			boom.add(new Explosion(alien.get(i).getX(), alien.get(i).getY()));
    			alien.remove(i);    			
    			spaceship.addScore(8);
    		}
    		else if (enemy.contains(shippoint)) {
    			alien.remove(i);
    			spaceship.TakeDamage(10+(4*difficult));
    		}
    	}
    	for (int i=0; i<lifebonus.size(); i++) {
    		if (lifebonus.get(i).getBounds().contains(shippoint)) {
    			spaceship.RecoveryShield();
    			lifebonus.remove(i);
    		}
    	}
    	for (int i=0; i<scorebonus.size(); i++) {
    		if (scorebonus.get(i).getBounds().contains(shippoint)) {
    			spaceship.addScore(100);
    			scorebonus.remove(i);
    		}
    	}
    	for (int i=0; i<enemyshot.size(); i++) {
    		enemy = enemyshot.get(i).getBounds();
    		if (enemy.contains(shippoint)) {
    			enemyshot.remove(i);
    			spaceship.TakeDamage(7+(3*difficult));
    		}
    	}
    }
  
    private boolean ColidesBullet(Rectangle enemy) {
    	Rectangle shot;
    	for (int i=0; i<spaceship.bullet.size(); i++) {
    		shot = spaceship.bullet.get(i).getBounds();
    		if (shot.intersects(enemy)) {
    			spaceship.bullet.remove(i);
    			return true;
    		}
    	}
    	return false;
    }

    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
        	int key = e.getKeyCode();
            spaceship.keyPressed(e);
            if (key == KeyEvent.VK_P) {
            	if (ispaused) ispaused = false;
            	else ispaused = true;
            }
            if ((spaceship.getShield() <= 0 || ispaused) && key == KeyEvent.VK_ENTER) {
            	InitAttributes();
            	ispaused = false;
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }

        
    }
    
}