package ep2;

public class Astronaut extends Sprite {

	private final static double SPEED=0.5;
	private int timex=0, timey=0;
	private int directionX, directionY;
	
	public Astronaut(int x, int y) {
		super(x, y);
		loadImage("images/Astronaut.gif");
	}
	
	public void move() {
		if (timex < 150) directionX=1;
		else if (timex < 300) directionX=-1;
		else timex=0;
		
		if (timey < 300) directionY=1;
		else if (timey < 490) directionY=-1;
		else timey=0;	
		
		timex++;
		timey++;
		
		super.move(directionX * SPEED, directionY * SPEED);
	}
}
