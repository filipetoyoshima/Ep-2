package ep2;

public class Explosion extends Sprite {
	private final static int FADETIME=80;
	private int lifetime=0, frame=1;
	
	public Explosion(int x, int y) {
		super(x, y);
		loadImage("images/explosion/f1.png");
	}

	public boolean Finish() {
		if (lifetime >= FADETIME) return true;
		else {
			lifetime++;
			frame=lifetime/3;
			loadImage("images/explosion/f" + frame + ".png");
			return false;
		}
	}
}
