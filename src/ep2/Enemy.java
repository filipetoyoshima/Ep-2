package ep2;

public abstract class Enemy extends Sprite {
	
	protected Sprite target;
	protected int shottimer=100;
	protected int shotlate=400;
	protected double bulletspeed=1;
	
	public Enemy(int x, int y, Sprite target) {
		super(x, y);
		this.target = target;
	}
		
	public void Update() {
		shottimer++;
	}
	
	public boolean ShotIsReady() {
		if (shottimer >= shotlate) {
			shottimer = 0;
			return true;
		} else return false;
	}
	
	public double getBulletSpeed() {
		return bulletspeed;
	}
}
