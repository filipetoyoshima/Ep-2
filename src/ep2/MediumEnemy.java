package ep2;

public class MediumEnemy extends Enemy {
	
	private static int BULLETSPEED = 2;
	//double timer=0;
	private int movetimer=0;
	public MediumEnemy(int x, int y, Sprite target) {
		super(x, y, target);
		loadImage("images/RedOVNI.gif");
		shotlate = 150;
		bulletspeed = BULLETSPEED;
	}
	
	public void Update () {
        	y+=1;
        	/* x+=Math.cos(timer);
        	timer+=Math.PI/40;
        	if (timer > 2 * Math.PI) timer = 0; */
        	if (movetimer < 50) x-=2;
        	else if (movetimer < 100) x+=2;
        	else movetimer=0;
        	movetimer++;
        	super.Update();
	}
}
