package ep2;

public class Selecter extends Sprite {
		
	public Selecter(int x, int y, String image) {
		super(x, y);
		loadImage("images/" + image);
	}
	
	public void setPosition(int x, int y) {
		this.x=x;
		this.y=y;
	}
}
